import os
import platform

global studentlist
studentlist = ["Redouane essadik", "christophe grec", "alexandre arnold", "Jesse lingard"]

def main():
    while 1:
        print("Bienvenue au systeme de management des inscription")
        print("")
        print("1. Login as admin")

        user_option = input(str("Option : "))
        if user_option == "1":
            auth_admin()
        else:
            print("No valid option was selected")
            
def auth_admin():
    print("")
    print("Admin Login")
    print("")
    username = input(str("Username : "))
    password = input(str("Password : "))
    if username == "Redouane":
        if password == "essadik":
            admin_session()
        else:
            print("Votre Mot de passe incorrect !")
    else:
        print("Les données de connexion ne sont pas reconnues ") 

def admin_session():
    while 1:
        print("")
        print("Menu admin")
        print("1. Affichage de la liste des étudiants")
        print("2. Ajouter un nouvel etudiant")
        print("3. Recherche d'un étudiant")
        print("4. Suppression d'un étudiant]\n")
        print("5. Déconnexion")

        try:
            x = int(input("Entrer votre choix: "))
        except ValueError:
                exit("\nHy! Ce n'est pas un numéro")
        else:
                print("\n")

        if(x==1):
                print("Liste des étudiants\n")
                for students in studentlist:
                    print("++ {} ++".format(students))

        elif(x==2):
                studentnew = input("Entrer un nouvel étudiant: ")
                if(studentnew in studentlist):
                    print("\nCet étudiant {} est déja sur la table".format(studentnew))
                else:
                        studentlist.append(studentnew)
                        print("\n++ Etudiant {} ajouté ++\n".format(studentnew))
                        for students in studentlist:
                            print("++ {} ++".format(students))

        elif(x==3):
                studentsearching = input("Choisissez le nom de l'étudiant à rechercher: ")
                if(studentsearching in studentlist):
                        print("\n++ Il y a un dossier trouvé pour cet étudiant {} ++".format(studentsearching))
                else:
                        print("\n++ Il n'y a aucune trace de cet étudiant {} ++".format(studentsearching))

        elif(x==4):
                studentdelete = input("Choisissez un nom d'étudiant à supprimer: ")
                if(studentdelete in studentlist):
                        studentlist.remove(studentdelete)
                        for students in studentlist:
                            print("++ {} ++".format(students))
                else:
                        print("\n++ Il n'y a pas de dossier trouvé pour cet étudiant {} ++".format(studentdelete))

        elif(x < 1 or x > 4):
                  print("Entrer un choix valide")

main()

def continueAgain():
	runningagain = input("\nVoulez-vous continuer le processus oui/non ?: ")
	if(runningagain.lower() == 'oui'):
		if(platform.system() == "Windows"):
			print(os.system('cls'))
		else:
			print(os.system('clear'))
		admin_session()
		continueAgain()
	else:
		quit()

continueAgain()



	


